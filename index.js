console.log("Hello World")

// Addition Function
function addition(num1, num2){
	let sumNum = num1 + num2
	console.log("Displayed sum of " + num1 + " and " + num2)
	console.log(sumNum)
}

addition(5,15)

// Subtraction Function
function subtract(num1, num2){
	let diffNum = num1 - num2
	console.log("Displayed difference of " + num1 + " and " + num2)
	console.log(diffNum)
}

subtract(20,5)

// Multiplication Function
function multiply(num1, num2){
	console.log("The product of " + num1 + " and " + num2 + ":")
	let multiplyNum = num1 * num2
	return multiplyNum
}

let product = multiply(50,10);
console.log(product)

// Division Function
function divide(num1, num2){
	console.log("The product of " + num1 + " and " + num2 + ":")
	let divideNum = num1 / num2
	return divideNum
}

let quotient = divide(50,10)
console.log(quotient)


// Circle Area Function
function areaCircle(radius){
	console.log("The result of getting the area of a circle with " + radius + " radius:")
	let formulaCircle = Math.PI * radius ** 2;
	return formulaCircle
}

let circleArea = areaCircle(15);
console.log(circleArea);

// Average Function
function averageNum(num1,num2,num3,num4){
	console.log("The average of " + num1 + ", " + num2 + "," + num3 + " and " + num4 + ":" )
	let averageCalc = (num1 + num2 + num3 + num4)/4 ;
	return averageCalc
}

let averageVar = averageNum(20,40,60,80);
console.log(averageVar);


// Passing Function
function passingScore(yourScore, totalScore){
	console.log("Is " + yourScore + "/" + totalScore + " a passing score?" )
	let isPassed = yourScore/totalScore * 100 > 75
	return isPassed
}

let isPassingScore = passingScore(38,50);
console.log(isPassingScore);